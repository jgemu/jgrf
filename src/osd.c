/*
Copyright (c) 2020-2025 Rupert Carmichael
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "jgrf.h"
#include "osd.h"
#include "settings.h"

#include "font/6x8.h"
#include "font/8x8.h"

static uint8_t *font;
static unsigned fwidth = 8;
static unsigned jgorange = 0x00d45500;

static jg_videoinfo_t *v = NULL;
static jg_setting_t *settings = NULL;

static void (*jgrf_osd_drawpix)(unsigned, unsigned);

static void jgrf_osd_drawpix32(unsigned drawoffset, unsigned set) {
    uint32_t *vbuf = (uint32_t*)v->buf;
    if (set) {
        vbuf[drawoffset] = jgorange;
    }
    else {
        uint8_t r = (vbuf[drawoffset] & 0xff0000) >> 18;
        uint8_t g = (vbuf[drawoffset] & 0xff00) >> 10;
        uint8_t b = (vbuf[drawoffset] & 0xff) >> 2;
        vbuf[drawoffset] = (unsigned)((r << 16) | (g << 8) | b);
    }
}

static void jgrf_osd_drawpix5551(unsigned drawoffset, unsigned set) {
    uint16_t *vbuf = (uint16_t*)v->buf;
    if (set) {
        vbuf[drawoffset] = jgorange;
    }
    else {
        uint8_t r = (vbuf[drawoffset] & 0xf800) >> 13;
        uint8_t g = (vbuf[drawoffset] & 0x7c0) >> 8;
        uint8_t b = (vbuf[drawoffset] & 0x3e) >> 2;
        vbuf[drawoffset] = (unsigned)((r << 11) | (g << 6) | b);
    }
}

static void jgrf_osd_drawpix565(unsigned drawoffset, unsigned set) {
    uint16_t *vbuf = (uint16_t*)v->buf;
        if (set) {
        vbuf[drawoffset] = jgorange;
    }
    else {
        uint8_t r = (vbuf[drawoffset] & 0xf800) >> 13;
        uint8_t g = (vbuf[drawoffset] & 0x7e0) >> 7;
        uint8_t b = (vbuf[drawoffset] & 0x1f) >> 2;
        vbuf[drawoffset] = (unsigned)((r << 11) | (g << 5) | b);
    }
}

void jgrf_osd_render(int xo, int yo, const char *text) {
    unsigned xoffset = xo;
    unsigned yoffset = yo;
    unsigned set = 0;

    for (size_t c = 0; c < strlen(text); ++c) {
        if (text[c] == '\n') {
            yoffset += 8;
            xoffset = xo;
            continue;
        }

        if ((xoffset - xo) + fwidth > v->w)
            continue;

        uint8_t *bitmap = &font[text[c] << 3];

        for (unsigned x = 0; x < fwidth; ++x) {
            for (unsigned y = 0; y < 8; ++y) {
                set = bitmap[y] & (1 << (7 - x));
                unsigned drawoffset = ((y + yoffset) * v->wmax) + x + xoffset;
                jgrf_osd_drawpix(drawoffset, set);
            }
        }
        xoffset += fwidth;
    }
}

void jgrf_osd_rehash(void) {
    switch (settings[MISC_FONT].val) {
        default: case 0: { // 6x8
            font = sixbyeight;
            fwidth = 6;
            break;
        }
        case 1: { // 8x8
            font = eightbyeight;
            fwidth = 8;
            break;
        }
    }
}

void jgrf_osd_init(jg_videoinfo_t *vidinfo) {
    v = vidinfo;
    settings = jgrf_settings_ptr();

    switch (v->pixfmt) {
        case JG_PIXFMT_XRGB8888: {
            jgorange = 0x00d45500;
            jgrf_osd_drawpix = &jgrf_osd_drawpix32;
            break;
        }
        case JG_PIXFMT_XBGR8888: {
            jgorange = 0x000055d4;
            jgrf_osd_drawpix = &jgrf_osd_drawpix32;
            break;
        }
        case JG_PIXFMT_RGBX5551: {
            jgorange = 0xd280;
            jgrf_osd_drawpix = &jgrf_osd_drawpix5551;
            break;
        }
        case JG_PIXFMT_RGB565: {
            jgorange = 0xd2a0;
            jgrf_osd_drawpix = &jgrf_osd_drawpix565;
            break;
        }
        default: break;
    }

    jgrf_osd_rehash();
}
